import { combineReducers } from "redux"
import { authReducer } from "./authReducer"
import { chatsReducer } from "./chatsReducer"
import { chatReducer } from "./chatReducer"

export const rootReducer = combineReducers({
	auth: authReducer,
	chats: chatsReducer,
	chat: chatReducer
})
