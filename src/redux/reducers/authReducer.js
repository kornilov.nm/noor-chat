import {
	HIDE_LOADING,
	LOGIN_ERROR,
	LOGIN_SUCCESS,
	REGISTER_ERROR,
	REGISTER_SUCCESS,
	RESET_ERROR,
	SHOW_LOADING,
	SIGN_OUT_SUCCESS,
} from "../types/typesAuth"

const initialState = {
	loading: false,
	credentials: null,
	token: null,
	authError: null,
}

export const authReducer = (state = initialState, action) => {
	switch (action.type) {
	case SHOW_LOADING: {
		return {
			...state,
			loading: true,
		}
	}
	case HIDE_LOADING:
		return {
			...state,
			loading: false,
		}
	case LOGIN_SUCCESS:
		return {
			...state,
			credentials: action.userCredentials,
			token: action.token,
		}
	case LOGIN_ERROR:
		return {
			...state,
			authError: action.error,
		}
	case REGISTER_SUCCESS:
		return {
			...state,
		}
	case REGISTER_ERROR:
		return {
			...state,
			authError: action.error,
		}
	case RESET_ERROR:
		return {
			...state,
			authError: null,
		}
	case SIGN_OUT_SUCCESS:
		return {
			...state,
			token: null,
			credentials: null,
		}
	default:
		return state
	}
}
