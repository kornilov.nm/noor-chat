import { CHANGE_MESSAGE, SET_TYPING, SHOW_TYPING, UPDATE_MESSAGES } from "../types/typesChat"

const initialState = {
	messages: [],
	loading: false,
	message: "",
	isTyping: false,
	showTyping: false,
}

export const chatReducer = (state = initialState, action) => {
	switch (action.type) {
	case UPDATE_MESSAGES:
		return{
			...state,
			messages: action.messages,
		}
	case CHANGE_MESSAGE:
		return {
			...state,
			message: action.message,
		}
	case SET_TYPING:
		return {
			...state,
			isTyping: action.isTyping,
		}
	case SHOW_TYPING:
		return  {
			...state,
			showTyping: action.showTyping,
		}
	default:
		return state
	}
}