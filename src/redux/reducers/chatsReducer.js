import {
	FETCH_CHATS_SUCCESS,
	HIDE_LOADING,
	NEXT_PAGE,
	PREV_PAGE,
	RESET_PAGE,
	RESET_SELECTED_CHAT,
	SELECT_CHAT,
	SET_FILTER,
	SET_TOTAL_CHATS,
	SHOW_LOADING,
	START_CHAT_SUCCESS,
	TOGGLE_SAVED_SUCCESS,
	UPDATE_QUEUE,
} from "../types/typesChats"

const initialState = {
	queue: {},
	queueLength: 0,

	chats: {},
	selectedChat: {},
	totalChats: 0,

	currentPage: 1,
	perPage: 4,

	filter: "",
	loading: false,
}

export const chatsReducer = (state = initialState, action) => {
	switch (action.type) {
	case FETCH_CHATS_SUCCESS:
		return {
			...state,
			chats: action.chats,
		}
	case UPDATE_QUEUE:
		return {
			...state,
			queue: action.queue,
			queueLength: action.queue ? Object.keys(action.queue).length : 0,
		}
	case SET_FILTER:
		return {
			...state,
			filter: action.filter,
			currentPage: 1,
		}
	case SHOW_LOADING:
		return {
			...state,
			loading: true,
		}
	case HIDE_LOADING:
		return {
			...state,
			loading: false,
		}
	case NEXT_PAGE:
		return {
			...state,
			currentPage: state.currentPage + 1,
		}
	case PREV_PAGE:
		return {
			...state,
			currentPage: state.currentPage - 1,
		}
	case RESET_PAGE:
		return {
			...state,
			currentPage: 1,
		}
	case TOGGLE_SAVED_SUCCESS:
		return {
			...state,
			chats: action.chats,
		}
	case START_CHAT_SUCCESS:
		return {
			...state,
			chats: action.chats,
			queue: action.queue,
		}
	case SELECT_CHAT:
		return {
			...state,
			selectedChat: action.chat,
		}
	case RESET_SELECTED_CHAT:
		return {
			...state,
			selectedChat: null,
		}
	case SET_TOTAL_CHATS:
		return {
			...state,
			totalChats: action.totalChats,
		}
	default:
		return state
	}
}
