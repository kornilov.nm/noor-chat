import {
	FETCH_CHATS_REQUEST,
	NEXT_PAGE,
	PREV_PAGE,
	RESET_PAGE,
	RESET_SELECTED_CHAT,
	SELECT_CHAT,
	SET_FILTER,
	SET_TOTAL_CHATS,
	START_CHAT_REQUEST,
	TOGGLE_SAVED_REQUEST,
	UPDATE_QUEUE,
} from "../types/typesChats"

export function setFilter(filter) {
	return {
		type: SET_FILTER,
		filter,
	}
}

export function updateQueue(queue) {
	return {
		type: UPDATE_QUEUE,
		queue,
	}
}

export function fetchChats(uid, token) {
	return {
		type: FETCH_CHATS_REQUEST,
		uid,
		token,
	}
}
export function toggleSaved(chats, chatId, token) {
	return {
		type: TOGGLE_SAVED_REQUEST,
		chats,
		chatId,
		token,
	}
}

export function nextPage() {
	return {
		type: NEXT_PAGE,
	}
}

export function prevPage() {
	return {
		type: PREV_PAGE,
	}
}

export function resetPage() {
	return {
		type: RESET_PAGE,
	}
}

export function startChat(
	chatId,
	uid,
	token,
	history,
	newChatsState,
	newQueueState
) {
	return {
		type: START_CHAT_REQUEST,
		chatId,
		uid,
		token,
		history,
		newChatsState,
		newQueueState,
	}
}

export function selectChat(chat) {
	return {
		type: SELECT_CHAT,
		chat,
	}
}

export function resetSelectedChat() {
	return {
		type: RESET_SELECTED_CHAT,
	}
}
export function setTotalChats(totalChats) {
	return {
		type: SET_TOTAL_CHATS,
		totalChats,
	}
}
