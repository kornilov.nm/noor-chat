import {
	LOGIN_REQUEST,
	REGISTER_REQUEST,
	RESET_PASSWORD_REQUEST,
	SIGN_OUT_REQUEST,
	SOCIAL_MEDIA_AUTH_REQUEST,
	VERIFY_PASSWORD_REQUEST,
	VERIFY_TOKEN_REQUEST,
} from "../types/typesAuth"

export function loginRequest(user, history) {
	return {
		type: LOGIN_REQUEST,
		user,
		history,
	}
}

export function registerRequest(newUser, history) {
	return {
		type: REGISTER_REQUEST,
		newUser,
		history,
	}
}

export function socialMediaAuthRequest(mediaProvider, history) {
	return {
		type: SOCIAL_MEDIA_AUTH_REQUEST,
		mediaProvider,
		history
	}
}

export function resetPasswordRequest(userEmail) {
	return {
		type: RESET_PASSWORD_REQUEST,
		userEmail,
	}
}
export function verifyPasswordRequest(actionCode, newPassword, history) {
	return {
		type: VERIFY_PASSWORD_REQUEST,
		actionCode,
		newPassword,
		history,
	}
}

export function verifyTokenRequest(uid, token, history) {
	return {
		type: VERIFY_TOKEN_REQUEST,
		uid,
		token,
		history,
	}
}

export function signOutRequest(history) {
	return {
		type: SIGN_OUT_REQUEST,
		history,
	}
}
