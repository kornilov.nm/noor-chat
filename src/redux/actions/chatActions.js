import { CHANGE_MESSAGE, SET_TYPING, SHOW_TYPING, UPDATE_MESSAGES } from "../types/typesChat"

export function updateMessages(messages) {
	return {
		type: UPDATE_MESSAGES,
		messages,
	}
}

export function changeMessage(message) {
	return {
		type: CHANGE_MESSAGE,
		message,
	}
}

export function setTyping(isTyping) {
	return {
		type: SET_TYPING,
		isTyping,
	}
}

export function setShowTyping(showTyping) {
	return {
		type: SHOW_TYPING,
		showTyping,
	}
}
