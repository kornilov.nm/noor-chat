import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import "react-toastify/dist/ReactToastify.css"
import App from "./App"
import { Provider } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import reportWebVitals from "./reportWebVitals"
import "./firebase"
import { PersistGate } from "redux-persist/integration/react"
import Store from "./configureStore"
import { PubNubProvider } from "pubnub-react"
import pubnub from "./pubnub"

const {store, persistor} = Store()

const app = (
	<React.StrictMode>
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<PubNubProvider client={pubnub}>
					<BrowserRouter>
						<App />
					</BrowserRouter>
				</PubNubProvider>
			</PersistGate>
		</Provider>
	</React.StrictMode>
)
ReactDOM.render(app, document.getElementById("root"))

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
