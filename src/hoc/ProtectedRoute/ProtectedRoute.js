import React from "react"
import { Redirect, Route } from "react-router-dom"
import { useSelector } from "react-redux"
import propTypes from "prop-types"

const ProtectedRoute = ({ component: Component, ...restOfProps }) => {
	const isAuthenticated = useSelector((state) => !!state?.auth.credentials)
	return (
		<Route
			{...restOfProps}
			render={(props) =>
				isAuthenticated ? <Component {...props} /> : <Redirect to="auth" />
			}
		/>
	)
}

ProtectedRoute.propTypes = {
	component: propTypes.func,
}
export default ProtectedRoute
