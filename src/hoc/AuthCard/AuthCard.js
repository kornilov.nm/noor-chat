import React from "react"
import PropTypes from "prop-types"

const AuthCard = (props) => {
	return <div className="bg-white rounded p-4">{props.children}</div>
}

AuthCard.propTypes = {
	children: PropTypes.element,
}
export default AuthCard
