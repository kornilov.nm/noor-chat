import React from "react"
import propTypes from "prop-types"
import {ToastContainer} from "react-toastify"

const Layout = (props) => {
	return (
		<>
			<ToastContainer />
			<main className="container-fluid bg-primary h-100">{props.children}</main>
		</>
	)
}

Layout.propTypes = {
	children: propTypes.element,
}
export default Layout
