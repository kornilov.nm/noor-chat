import createSagaMiddleware from "redux-saga"
import { applyMiddleware, compose, createStore } from "redux"
import { persistStore, persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"

import { rootReducer } from "./redux/reducers/rootReducer"
import rootSaga from "./sagas/rootSaga"

const rootPersistConfig = {
	key: "root",
	storage,
	whitelist: ["auth"]
}
const persistedReducer = persistReducer(rootPersistConfig, rootReducer)

const Store = () => {
	const saga = createSagaMiddleware()
	const store = createStore(
		persistedReducer,
		compose(
			applyMiddleware(saga),
			window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
		)
	)
	const persistor = persistStore(store)
	saga.run(rootSaga)
	return {store, persistor}
}
export default Store
