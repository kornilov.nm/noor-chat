import React from "react"
import { Redirect, Route, Switch } from "react-router-dom"
import Auth from "./containers/Auth/Auth"
import Layout from "./hoc/Layout/Layout"
import Main from "./containers/Main/Main"
import ProtectedRoute from "./hoc/ProtectedRoute/ProtectedRoute"

function App() {
	const routes = (
		<Switch>
			<Route path="/auth" component={Auth} />
			<ProtectedRoute path="/" component={Main} />
			<Redirect to="/" />
		</Switch>
	)

	return <Layout>{routes}</Layout>
}

export default App
