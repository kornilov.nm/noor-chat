import firebase from "../firebase"

async function firebaseLogin(user) {
	return await firebase
		.auth()
		.signInWithEmailAndPassword(user.email, user.password)
}
async function firebaseRegister(newUser) {
	return await firebase
		.auth()
		.createUserWithEmailAndPassword(newUser.email, newUser.password)
}

async function socialMediaAuth(provider) {
	return await firebase.auth().signInWithPopup(provider)
}

async function resetPassword(userEmail) {
	return await firebase.auth().sendPasswordResetEmail(userEmail)
}

async function verifyPasswordResetCode(actionCode) {
	return await firebase.auth().verifyPasswordResetCode(actionCode) // return Email
}
async function confirmPasswordReset(actionCode, newPassword) {
	return await firebase.auth().confirmPasswordReset(actionCode, newPassword) //returns res
}

async function getToken() {
	return await firebase.auth().currentUser.getIdToken()
}
async function verifyToken(uid, token) {
	const req = await fetch(
		`https://noor-chat-default-rtdb.europe-west1.firebasedatabase.app/users/${uid}.json?auth=${token}`
	)
	const res = req.json()
	if (req.ok) {
		return res
	}
	throw new Error("Token is invalid or expired")
}

export {
	firebaseLogin,
	firebaseRegister,
	socialMediaAuth,
	resetPassword,
	verifyPasswordResetCode,
	confirmPasswordReset,
	verifyToken,
	getToken,
}
