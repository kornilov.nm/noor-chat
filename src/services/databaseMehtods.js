import axios from "axios"

export async function fetchChatsFirebase(uid, token) {
	return axios.get(
		`https://noor-chat-default-rtdb.europe-west1.firebasedatabase.app/chats.json/
				?auth=${token}
				&orderBy="operatorId"
				&equalTo="${uid}"`
	)
}
export async function patchChatFirebase(chatId, newValue, token) {
	return axios.patch(
		`https://noor-chat-default-rtdb.europe-west1.firebasedatabase.app/chats/${chatId}.json/?auth=${token}`,
		{ saved: newValue }
	)
}
export async function startChatFirebase(chatId, operatorId, token) {
	return axios.patch(
		`https://noor-chat-default-rtdb.europe-west1.firebasedatabase.app/chats/${chatId}.json/?auth=${token}`,
		{ operatorId: operatorId, status: "active"}
	)
}

