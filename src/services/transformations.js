export function formatChats(chats) {
	return chats && chats.length !== 0 ? Object.entries(chats).map((chat) => {
		const id = chat[0]
		const data = chat[1]
		data.chatId = id
		return data
	}
	) : []
}