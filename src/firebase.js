import firebase from "firebase/app"
import "firebase/auth"
import "firebase/database"

const firebaseConfig = {
	apiKey: process.env.REACT_APP_FB_API_KEY,
	authDomain: "noor-chat.firebaseapp.com",
	projectId: "noor-chat",
	storageBucket: "noor-chat.appspot.com",
	messagingSenderId: "841463991406",
	appId: "1:841463991406:web:9630ca9371dabd1528ea7f",
	databaseURL:
		"https://noor-chat-default-rtdb.europe-west1.firebasedatabase.app",
	measurementId: "G-F11KVMMPHF",
}

firebase.initializeApp(firebaseConfig)

export default firebase
