import React from "react"
import { Input } from "reactstrap"
import propTypes from "prop-types"
import { usePubNub } from "pubnub-react"
import firebase from "../../firebase"
import { useDispatch, useSelector } from "react-redux"
import { changeMessage, setTyping } from "../../redux/actions/chatActions"

const ChatInput = (props) => {
	const chatId = props.chatId
	const isDisabled = props.disabled
	const dispatch = useDispatch()
	const message = useSelector((state) => state.chat.message)
	const messages = useSelector((state) => state.chat.messages)
	const isTyping = useSelector((state) => state.chat.isTyping)
	const pubnub = usePubNub()

	const cls = [
		"position-absolute bottom-0 w-100 bg-secondary d-flex align-items-center justify-content-around",
	]
	if (isDisabled) {
		cls.push("opacity-50")
	}
	const signalStartTyping = () => {
		pubnub.signal(
			{
				message: "typing_on_operator",
				channel: chatId,
			},
			(status) => {
				if (status.error) {
					console.error(status)
				}
			}
		)
	}
	const signalStopTyping = () => {
		pubnub.signal(
			{
				message: "typing_off_operator",
				channel: chatId,
			},
			(status) => {
				if (status.error) {
					console.error(status)
				}
			}
		)
	}

	const inputHandler = (event) => {
		const msg = event.target.value
		dispatch(changeMessage(msg))

		if (msg.length > 0 && !isTyping) {
			dispatch(setTyping(true))
			signalStartTyping()
		}
		if (msg.length === 0 && isTyping) {
			dispatch(setTyping(false))
			signalStopTyping()
		}
	}
	const handleInputKey = (event) => {
		if (event.key === "Enter") {
			sendMessageHandler()
		}
	}

	const sendMessageHandler = () => {
		const messageToSend = {
			content: message,
			timestamp: Date.now(),
			writtenBy: "operator",
		}
		const messageContent = messageToSend.content
		if (typeof messageContent === "string" && messageContent.length > 0) {
			firebase
				.database()
				.ref(`chats/${chatId}`)
				.child("messages")
				.child(messages.length)
				.set(messageToSend)
				.then(() => {
					dispatch(changeMessage(""))
				})
				.catch((e) => {
					console.log(e)
				})
		}
	}

	return (
		<div className={cls.join(" ")} style={{ height: "10%" }}>
			<Input
				onChange={inputHandler}
				value={message}
				disabled={isDisabled}
				onKeyUp={handleInputKey}
				className="m-2 w-75"
			/>
			<i className="fa fa-2x fa-smile-o mx-3" />
			{/*<i className="fa fa-2x fa-paperclip mx-3" />*/}
			<i
				className="fa fa-2x fa-paper-plane mx-3 "
				onClick={sendMessageHandler}
				style={{ cursor: "pointer" }}
			/>
		</div>
	)
}

ChatInput.propTypes = {
	disabled: propTypes.bool,
	chatId: propTypes.string,
}

export default ChatInput
