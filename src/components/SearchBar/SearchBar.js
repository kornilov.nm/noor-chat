import React, { useCallback, useMemo } from "react"
import { Input } from "reactstrap"
import { useDispatch } from "react-redux"
import debounce from "lodash.debounce"
import { setFilter } from "../../redux/actions/chatsActions"


const SearchBar = (props) => {
	const dispatch = useDispatch()

	const handleSearchInput = useCallback((event) => {
		dispatch(setFilter(event.target.value))
	}, [dispatch])
	
	const debouncedInput = useMemo(() => debounce(handleSearchInput, 400), [handleSearchInput])

	return (
		<div {...props} >
			<Input
				className="py-sm-1 py-0 m-0"
				name="email"
				type="search"
				placeholder="Search"
				onChange={(e) => debouncedInput(e)}
			/>
		</div>
	)
}

export default SearchBar
