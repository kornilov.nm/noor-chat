import React from "react"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import PrimaryButton from "../UI/Button/PrimaryButton"
import { signOutRequest } from "../../redux/actions/authActions"

const UserBar = (props) => {
	const history = useHistory()
	const dispatch = useDispatch()
	const userEmail = useSelector((state) => state.auth.credentials.user.email)
	const selectedChat = useSelector((state) => state.chats.selectedChat)

	const signOutHandler = () => {
		dispatch(signOutRequest(history))
	}

	return (
		<div {...props}>
			<div>
				<h3>{selectedChat ? selectedChat.clientName : ""}</h3>
			</div>
			<div className="">
				<h6>{userEmail}</h6>
				<PrimaryButton
					type="button"
					color="danger"
					className="py-1 py-sm-3 px-2 px-sm-3 align-self-center text-nowrap float-end d-none d-sm-block"
					onClick={signOutHandler}
				>
					Sign out
				</PrimaryButton>
			</div>
		</div>
	)
}

export default UserBar
