import React from "react"
import propTypes from "prop-types"

const TypingIndicator = (props) => {
	return(
		<div className="px-5 pb-1">
			<span>{ props.userName } is typing...</span>
		</div>
	)
}

TypingIndicator.propTypes = {
	userName: propTypes.string
}

export default TypingIndicator
