import React from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import { socialMediaAuthRequest } from "../../redux/actions/authActions"
import {
	facebookProvider,
	googleProvider,
} from "../../services/authProviders"

const SocialMediaAuthBar = () => {
	const dispatch = useDispatch()
	const history = useHistory()
	const iconClickHandler = (provider) => {
		dispatch(socialMediaAuthRequest(provider, history))
	}
	return (
		<div className="d-flex">
			<i
				className="fa fa-facebook-square display-5 me-3"
				style={{ color: "#4267B2", cursor: "pointer" }}
				onClick={() => iconClickHandler(facebookProvider)}
			/>
			<i
				className="fa fa-google display-5 me-3"
				style={{ color: "#F4B400", cursor: "pointer" }}
				onClick={() => iconClickHandler(googleProvider)}
			/>
		</div>
	)
}
export default SocialMediaAuthBar
