import React from "react"
import ChatList from "../../ChatList/ChatList"
import { useSelector } from "react-redux"
import { formatChats } from "../../../services/transformations"


const ChatsQueue = () => {
	const queue = useSelector((state) => state.chats.queue)

	const formattedQueue = formatChats(queue)

	return (
		<ChatList chats={formattedQueue} />
	)
}
export default ChatsQueue
