import React from "react"
import ChatList from "../../ChatList/ChatList"
import { useSelector } from "react-redux"
import Loader from "../../UI/Loader/Loader"
import { formatChats } from "../../../services/transformations"

const ChatsActive = () => {
	const chats = useSelector((state) => state.chats.chats)
	const loading = useSelector((state) => state.chats.loading)

	const formattedChats = formatChats(chats)

	const filteredChats =
		formattedChats.length !== 0
			? formattedChats.filter((chat) => {
				return chat.status === "active" && !chat.saved
			  })
			: []
	return loading ? <Loader color="white" /> : <ChatList chats={filteredChats} />
}
export default ChatsActive
