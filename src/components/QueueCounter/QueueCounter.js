import React from "react"
import { useSelector } from "react-redux"

const QueueCounter = () => {
	const length = useSelector((store) => store.chats.queueLength)
	return <h5 className="d-flex text-white">Clients in queue: {length}</h5>
}

export default QueueCounter
