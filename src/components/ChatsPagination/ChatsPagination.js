import React from "react"
import { Pagination, PaginationItem, PaginationLink } from "reactstrap"
import { nextPage, prevPage } from "../../redux/actions/chatsActions"
import { useDispatch, useSelector } from "react-redux"

const ChatsPagination = (props) => {
	const dispatch = useDispatch()
	const currentPage = useSelector((state) => state.chats.currentPage)
	const totalChats = useSelector((state) => state.chats.totalChats)
	const perPage = useSelector((state) => state.chats.perPage)

	const pageCount = totalChats ? Math.ceil(totalChats / perPage) : 0

	const toNextPage = (event) => {
		event.preventDefault()
		if (currentPage < pageCount) {
			dispatch(nextPage())
		}
	}
	const toPrevPage = (event) => {
		event.preventDefault()
		if (currentPage !== 1) {
			dispatch(prevPage())
		}
	}
	return pageCount > 1 ?
		<div {...props}>
			<Pagination className="justify-content-center d-flex ">
				<PaginationItem className="mx-1">
					<PaginationLink previous onClick={toPrevPage} />
				</PaginationItem>
				<PaginationItem>
					<PaginationLink next onClick={toNextPage} />
				</PaginationItem>
			</Pagination>

			<span className="me-2 h6">{currentPage} / {pageCount}</span>
		</div>
		: ""
}

export default ChatsPagination
