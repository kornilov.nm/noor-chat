import React from "react"
import propTypes from "prop-types"
import { NavLink } from "react-router-dom"

const SideBarButton = (props) => {
	const iconName = props.iconName

	const icnCls = ["fa"]
	if (iconName) {
		icnCls.push(iconName)
	}

	return (
		<NavLink
			exact
			to={props.navTo}
			onClick={props.onClick}
			activeClassName="bg-white text-primary rounded-3"
			className="text-link text-white"
		>
			<div className="btn btn-primary d-flex flex-column justify-content-center text-center">
				<i className={icnCls.join(" ")} />
				<h6>{props.children}</h6>
			</div>
		</NavLink>
	)
}

SideBarButton.propTypes = {
	children: propTypes.string,
	iconName: propTypes.string,
	navTo: propTypes.string,
	onClick: propTypes.func,
}
export default SideBarButton
