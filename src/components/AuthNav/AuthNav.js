import React from "react"
import propTypes from "prop-types"
import { NavLink } from "react-router-dom"
import { Nav, Navbar, NavItem } from "reactstrap"

const AuthNav = (props) => {
	const renderLinks = props.links.map((link, idx) => {
		return (
			<NavItem key={idx}>
				<NavLink className="btn btn-info" to={link.to} exact={link.exact}>
					{link.label}
				</NavLink>
			</NavItem>
		)
	})
	return (
		<Navbar className="mr-auto flex-row w-100 justify-content-center">
			<Nav className="mr-auto flex-row w-100 justify-content-between" navbar>
				{renderLinks}
			</Nav>
		</Navbar>
	)
}

AuthNav.propTypes = {
	links: propTypes.array,
}
export default AuthNav
