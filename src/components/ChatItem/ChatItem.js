import React from "react"
import propTypes from "prop-types"
import { useDispatch, useSelector } from "react-redux"
import SaveButton from "../SaveButton/SaveButton"
import { useHistory, useRouteMatch } from "react-router-dom"
import { startChat, toggleSaved } from "../../redux/actions/chatsActions"
import PrimaryButton from "../UI/Button/PrimaryButton"
import moment from "moment"
import Rating from "../UI/Rating/Rating"

const chatStatuses = {
	active: "list-group-item-warning",
	completed: "list-group-item-success",
	saved: "list-group-item-info",
}

const ChatItem = (props) => {
	const dispatch = useDispatch()
	const chats = useSelector((state) => state.chats.chats)
	const queue = useSelector((state) => state.chats.queue)
	const token = useSelector((state) => state.auth.token)
	const uid = useSelector((state) => state.auth.credentials.user.uid)
	const history = useHistory()
	const { url } = useRouteMatch()

	const chat = props.chat
	const inQueue = chat.status === "pending"
	const isCompleted = chat.status === "completed"
	const lastMsg = chat.messages[chat.messages.length - 1]

	const formattedData = moment(lastMsg.timestamp).fromNow()

	const cls = [
		"list-group-item list-group-item-action d-flex align-items-center mb-1 p-2",
	]
	if (chat.status && chatStatuses[chat.status]) {
		cls.push(chatStatuses[chat.status])
	}

	const toggleSaveHandler = (event) => {
		event.preventDefault()
		event.stopPropagation()

		const newChatsState = { ...chats }
		const isSaved = chat.saved

		newChatsState[chat.chatId].saved = !isSaved

		dispatch(toggleSaved(newChatsState, chat.chatId, token))
	}

	const chatClickHandler = (event) => {
		if (!inQueue) {
			event.stopPropagation()
			history.push(`${url}/${chat.chatId}`)
		}
	}
	const startChatHandler = (event) => {
		event.preventDefault()
		event.stopPropagation()

		const newQueueState = { ...queue }
		const newChat = newQueueState[chat.chatId]
		delete newQueueState[chat.chatId]
		newChat.operatorId = uid
		newChat.status = "active"
		const newChatsState = { ...chats }
		newChatsState[chat.chatId] = newChat

		dispatch(
			startChat(chat.chatId, uid, token, history, newChatsState, newQueueState)
		)
	}

	return (
		<li
			onClick={chatClickHandler}
			className={cls.join(" ")}
			style={{ cursor: "pointer" }}
		>
			<div className="w-100 d-flex flex-column">
				<div className="d-flex justify-content-between">
					<h6>{chat.clientName}</h6>
					<small>{formattedData}</small>
				</div>
				<p>{chat.title}</p>

				<div
					className={`d-flex align-items-center ${
						isCompleted ? "justify-content-between " : "align-self-end"
					}`}
				>
					{isCompleted ? <Rating rating={chat.rating} size={20} /> : ""}
					{inQueue ? (
						<PrimaryButton onClick={startChatHandler}>Start Chat</PrimaryButton>
					) : (
						<SaveButton isSaved={chat.saved} onClick={toggleSaveHandler} />
					)}
				</div>
			</div>
		</li>
	)
}

ChatItem.propTypes = {
	chat: propTypes.object,
}
export default ChatItem
