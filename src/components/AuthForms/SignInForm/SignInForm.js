import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { useFormik } from "formik"
import { Alert, Form, FormGroup } from "reactstrap"
import * as Yup from "yup"
import TextInput from "../../UI/TextInput/TextInput"
import PrimaryButton from "../../UI/Button/PrimaryButton"
import { loginRequest } from "../../../redux/actions/authActions"
import AuthNav from "../../AuthNav/AuthNav"
import SocialMediaAuthBar from "../../SocialMediaAuthBar/SocialMediaAuthBar"

const SignInForm = () => {
	const dispatch = useDispatch()
	const authError = useSelector((state) => state.auth.authError)
	const loading = useSelector((state) => state.auth.loading)
	const history = useHistory()

	const links = [
		{ to: "/auth/signup", label: "Sign up", exact: true },
		{ to: "/auth/recover", label: "Forgot Password", exact: true },
	]

	const formik = useFormik({
		initialValues: {
			email: "",
			password: "",
		},
		validationSchema: Yup.object({
			email: Yup.string().email("Invalid email address").required("Required"),
			password: Yup.string().required("Please Enter your password"),
		}),
		onSubmit: (values, actions) => {
			if (formik.isSubmitting) {
				const user = { email: values.email, password: values.password }
				dispatch(loginRequest(user, history))
				values.password = ""
			}
			actions.setSubmitting(false)
		},
	})
	return (
		<>
			<h2>Sign in</h2>
			<Form onSubmit={formik.handleSubmit}>
				<FormGroup>
					<TextInput
						label="Email"
						name="email"
						type="text"
						placeholder="Email"
						{...formik.getFieldProps("email")}
						meta={formik.getFieldMeta("email")}
					/>
					<TextInput
						label="Password"
						name="password"
						type="password"
						placeholder="Password"
						autoComplete="true"
						{...formik.getFieldProps("password")}
						meta={formik.getFieldMeta("password")}
					/>
					<SocialMediaAuthBar />
					<PrimaryButton
						type="submit"
						color="primary"
						className="w-100 py-3 align-self-center"
						disabled={loading || !formik.isValid}
					>
						Sign In
					</PrimaryButton>
				</FormGroup>
				{authError ? (
					<Alert color="danger" className="my-1">
						Something went wrong
					</Alert>
				) : null}
			</Form>

			<AuthNav links={links} />
		</>
	)
}

export default SignInForm
