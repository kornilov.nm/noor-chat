import React from "react"
import { Form, FormGroup } from "reactstrap"
import { useFormik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import * as Yup from "yup"
import AuthNav from "../../AuthNav/AuthNav"
import TextInput from "../../UI/TextInput/TextInput"
import PrimaryButton from "../../UI/Button/PrimaryButton"
import { resetPasswordRequest } from "../../../redux/actions/authActions"

const ResetForm = () => {
	const dispatch = useDispatch()
	const loading = useSelector((state) => state.auth.loading)

	const links = [
		{ to: "/auth", label: "Sign in", exact: true },
		{ to: "/auth/signup", label: "Sign up", exact: true },
	]

	const formik = useFormik({
		initialValues: {
			email: "",
		},
		validationSchema: Yup.object({
			email: Yup.string().email("Invalid email address").required("Required"),
		}),
		onSubmit: (values, actions) => {
			if (formik.isSubmitting) {
				const userEmail = values.email
				dispatch(resetPasswordRequest(userEmail))
			}
			actions.setSubmitting(false)
		},
	})

	return (
		<>
			<h2>Reset Password</h2>
			<Form onSubmit={formik.handleSubmit}>
				<FormGroup>
					<TextInput
						label="Email"
						name="email"
						type="text"
						placeholder="Email"
						{...formik.getFieldProps("email")}
						meta={formik.getFieldMeta("email")}
					/>

					<PrimaryButton
						type="submit"
						color="primary"
						className="w-100 py-3 align-self-center"
						disabled={loading || !formik.isValid}
					>
						Send a password reset link
					</PrimaryButton>
				</FormGroup>
			</Form>
			<AuthNav links={links} />
		</>
	)
}

export default ResetForm
