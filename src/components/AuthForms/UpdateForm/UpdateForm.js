import React from "react"
import { Form, FormGroup } from "reactstrap"
import { useFormik } from "formik"
import * as Yup from "yup"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import TextInput from "../../UI/TextInput/TextInput"
import PrimaryButton from "../../UI/Button/PrimaryButton"
import { useQuery } from "../../../hooks/Querry.hook"
import { verifyPasswordRequest } from "../../../redux/actions/authActions"

const UpdateForm = () => {
	const dispatch = useDispatch()
	const query = useQuery()
	const loading = useSelector((state) => state.auth.loading)
	const history = useHistory()

	const formik = useFormik({
		initialValues: {
			password: "",
			confirmPassword: "",
		},
		validationSchema: Yup.object({
			password: Yup.string()
				.required("Please Enter your password")
				.matches(
					/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
					"Must Contain 8 Characters, One Uppercase, One Lowercase and One Number"
				),
			confirmPassword: Yup.string()
				.required("Please Enter your password again")
				.oneOf([Yup.ref("password"), null], "Passwords must match"),
		}),
		onSubmit: (values, actions) => {
			if (formik.isSubmitting) {
				const newPassword = values.confirmPassword
				const actionCode = query.get("oobCode")
				dispatch(verifyPasswordRequest(actionCode, newPassword, history))
				values.password = values.confirmPassword = ""
			}
			actions.setSubmitting(false)
		},
	})

	return (
		<>
			<h2>Update Password</h2>
			<Form onSubmit={formik.handleSubmit}>
				<FormGroup>
					<TextInput
						label="Password"
						name="password"
						type="password"
						placeholder="Password"
						autoComplete="true"
						{...formik.getFieldProps("password")}
						meta={formik.getFieldMeta("password")}
					/>

					<TextInput
						label="Confirm password"
						name="password"
						type="password"
						placeholder="Write your password again"
						autoComplete="true"
						{...formik.getFieldProps("confirmPassword")}
						meta={formik.getFieldMeta("confirmPassword")}
					/>

					<PrimaryButton
						type="submit"
						color="primary"
						className="w-100 py-3 align-self-center"
						disabled={loading || !formik.isValid}
					>
						Send a recovery link
					</PrimaryButton>
				</FormGroup>
			</Form>
		</>
	)
}

export default UpdateForm
