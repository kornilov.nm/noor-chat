import React from "react"
import AuthNav from "../../AuthNav/AuthNav"
import { useFormik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Alert, Form, FormGroup } from "reactstrap"
import * as Yup from "yup"
import TextInput from "../../UI/TextInput/TextInput"
import PrimaryButton from "../../UI/Button/PrimaryButton"
import { registerRequest } from "../../../redux/actions/authActions"

const SignUpForm = () => {
	const dispatch = useDispatch()
	const authError = useSelector((state) => state.auth.authError)
	const loading = useSelector((state) => state.auth.loading)
	const history = useHistory()

	const links = [
		{ to: "/auth", label: "Sign in", exact: true },
		{ to: "/auth/recover", label: "Forgot Password", exact: true },
	]

	const formik = useFormik({
		initialValues: {
			email: "",
			password: "",
			confirmPassword: "",
		},
		validationSchema: Yup.object({
			email: Yup.string().email("Invalid email address").required("Required"),
			password: Yup.string()
				.required("Please Enter your password")
				.matches(
					/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
					"Must Contain 8 Characters, One Uppercase, One Lowercase and One Number"
				),
			confirmPassword: Yup.string()
				.required("Please Enter your password again")
				.oneOf([Yup.ref("password"), null], "Passwords must match"),
		}),
		onSubmit: (values, actions) => {
			if (formik.isSubmitting) {
				const newUser = {
					email: values.email,
					password: values.confirmPassword,
				}
				dispatch(registerRequest(newUser, history))
				values.password = values.confirmPassword = ""
			}
			actions.setSubmitting(false)
		},
	})

	return (
		<>
			<h2>Sign up</h2>
			<Form onSubmit={formik.handleSubmit}>
				<FormGroup>
					<TextInput
						label="Email"
						name="email"
						type="text"
						placeholder="Email"
						{...formik.getFieldProps("email")}
						meta={formik.getFieldMeta("email")}
					/>
					<TextInput
						label="Password"
						name="password"
						type="password"
						placeholder="Password"
						autoComplete="true"
						{...formik.getFieldProps("password")}
						meta={formik.getFieldMeta("password")}
					/>
					<TextInput
						label="Confirm password"
						name="password"
						type="password"
						placeholder="Write your password again"
						autoComplete="true"
						{...formik.getFieldProps("confirmPassword")}
						meta={formik.getFieldMeta("confirmPassword")}
					/>
					<PrimaryButton
						type="submit"
						color="primary"
						className="w-100 py-3 align-self-center"
						disabled={loading || !formik.isValid}
					>
						Sign Up
					</PrimaryButton>
				</FormGroup>
			</Form>
			{authError ? <Alert color="danger">{authError.message}</Alert> : null}

			<AuthNav links={links} />
		</>
	)
}

export default SignUpForm
