import React, { useEffect } from "react"
import propTypes from "prop-types"

import { useDispatch, useSelector } from "react-redux"
import ChatItem from "../ChatItem/ChatItem"
import { setTotalChats } from "../../redux/actions/chatsActions"

const ChatList = (props) => {
	const chats = props.chats
	const filter = useSelector((state) => state.chats.filter)
	const perPage = useSelector((state) => state.chats.perPage)
	const currentPage = useSelector((state) => state.chats.currentPage)
	const dispatch = useDispatch()

	const checkField = (field) => {
		return field.toLowerCase().includes(filter.toLowerCase())
	}
	const filteredChats = chats.filter((chat) => {
		let contains
		contains = checkField(chat.title) || checkField(chat.clientName)
		chat.messages.forEach(msg => {
			contains = checkField(msg.content)
		})
		return contains
	})


	const startIndex = perPage * (currentPage - 1)
	const endIndex = perPage * currentPage

	const paginatedChats = filteredChats.slice(startIndex, endIndex)

	const renderChats = paginatedChats.length
		? paginatedChats.map((chat) => {
			return <ChatItem chat={chat} key={chat.chatId} />
		  })
		: []

	useEffect(() => {
		dispatch(setTotalChats(filteredChats.length))
	}, [dispatch, filteredChats.length])

	return (
		<div className="d-flex flex-column justify-content-between">
			{renderChats.length ? (
				<ul className="list-group">{renderChats}</ul>
			) : (
				<div className="text-center">
					<div className="h3">Found Nothing</div>
					<div>
						Change you search filter or wait for new client&rsquo;s request
					</div>
				</div>
			)}
		</div>
	)
}

ChatList.propTypes = {
	chats: propTypes.array,
}
export default ChatList
