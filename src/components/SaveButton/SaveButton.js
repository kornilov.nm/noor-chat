import React from "react"
import propTypes from "prop-types"

const SaveButton = (props) => {
	const btnCls = ["btn"]
	const iconCls = ["fa"]
	if (props.isSaved) {
		btnCls.push("btn-danger")
		iconCls.push("fa-trash")
	} else {
		btnCls.push("btn-success")
		iconCls.push("fa-save")
	}

	return (
		<div onClick={props.onClick} className={btnCls.join(" ")}>
			<span className="me-1">{props.isSaved ? "Remove" : "Save"}</span>
			<i className={iconCls.join(" ")} />
		</div>
	)
}

SaveButton.propTypes = {
	isSaved: propTypes.bool,
	onClick: propTypes.func,
}
export default SaveButton
