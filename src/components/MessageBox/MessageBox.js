import React from "react"
import propTypes from "prop-types"
import UserMessage from "../UserMessage/UserMessage"
import UserAvatar from "../UserAvatar/UserAvatar"

const MessageBox = (props) => {
	const message = props.message
	const myMessage = message.writtenBy === "operator"

	const cls = ["d-flex flex-row w-100"]

	if (myMessage) {
		cls.push("float-end flex-row-reverse")
	}

	return (
		<div className={cls.join(" ")}>
			<UserAvatar />
			<UserMessage message={message} />
		</div>
	)
}

MessageBox.propTypes = {
	message: propTypes.object,
}
export default MessageBox
