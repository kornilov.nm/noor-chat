import React from "react"
import propTypes from "prop-types"
import Rating from "../UI/Rating/Rating"

const CompletedMessage = (props) => {
	const STARS_SIZE = 30
	const rating = props.rating

	return (
		<div className="d-flex flex-column w-100 mb-1 alert-info">
			<h5 className="text-center">Chat was completed</h5>
			<div className="w-100 d-flex justify-content-center align-items-center">
				<div className="h5 m-0 pt-1">Chat rating: </div>
				<Rating rating={rating} size={STARS_SIZE} />
			</div>
		</div>
	)
}

CompletedMessage.propTypes = {
	rating: propTypes.number,
}

export default CompletedMessage
