import React from "react"
import propTypes from "prop-types"
import moment from "moment"

const UserMessage = (props) => {
	const message = props.message

	const formattedTime = moment(message.timestamp ).format("h:mma")
	const formattedDay = moment(message.timestamp).fromNow()

	const isImage = message.content.includes("https://")
	return(
		<div className=" d-flex m-1 text-white flex-column" style={{ height: "fit-content"}} >
			<div className="bg-primary rounded">
				{
					isImage ?
						<img className="p-1" src={message.content} style={{maxWidth: "200px"}} alt={message.content}/>
						: <p className="p-2" style={{maxWidth: "300px",}}>{message.content} </p>
				}
			</div>
			<div className="d-flex justify-content-between">
				<span className="small text-black me-2">{formattedTime}</span>
				<span className="small text-black ">{formattedDay}</span>
			</div>
		</div>
	)
}

UserMessage.propTypes = {
	message: propTypes.object
}

export default UserMessage