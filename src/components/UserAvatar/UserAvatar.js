import React from "react"
import propTypes from "prop-types"

const UserAvatar = () => {
	return (
		<div className="d-flex flex-column justify-content-center text-center align-self-start pt-1">
			<div className="d-flex flex-column">
				<i className="fa fa-user-circle fa-3x" />
			</div>
		</div>
	)
}

UserAvatar.propTypes = {}

export default UserAvatar
