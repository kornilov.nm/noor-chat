import React from "react"
import { Spinner } from "reactstrap"
import propTypes from "prop-types"

const Loader = ({ color }) => {
	return (
		<div className="d-flex justify-content-center">
			<Spinner color={color} />
		</div>
	)
}
Loader.propTypes = {
	color: propTypes.string,
}
export default Loader
