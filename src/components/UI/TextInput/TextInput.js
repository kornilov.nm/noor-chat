import React from "react"
import propTypes from "prop-types"
import { Alert, Input, Label } from "reactstrap"

const TextInput = ({ label, meta, ...props }) => {
	const { touched, error } = meta

	return (
		<>
			<Label for={props.id || props.name}>{label}</Label>
			<Input className="my-2" {...props} />
			{touched && error ? <Alert color="danger">{error}</Alert> : null}
		</>
	)
}

TextInput.propTypes = {
	label: propTypes.string,
	id: propTypes.string,
	name: propTypes.string,
	meta: propTypes.object,
}
export default TextInput
