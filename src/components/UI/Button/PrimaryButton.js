import React from "react"
import propTypes from "prop-types"
import { Button } from "reactstrap"
const PrimaryButton = (props) => {
	return <Button {...props}>{props.children}</Button>
}

PrimaryButton.propTypes = {
	children: propTypes.string,
}
export default PrimaryButton
