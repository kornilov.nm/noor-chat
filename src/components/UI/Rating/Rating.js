import React from "react"
import propTypes from "prop-types"
import ReactStars from "react-rating-stars-component/dist/react-stars"

const Rating = (props) => {

	return(
		<ReactStars
			count={5}
			edit={false}
			size={props.size}
			value={props.rating}
			activeColor="#ffd700"
		/>
	)
}
Rating.propTypes = {
	rating: propTypes.number,
	size: propTypes.number
}
export default Rating