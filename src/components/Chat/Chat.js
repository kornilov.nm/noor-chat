import React, { useEffect } from "react"
import { useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import firebase from "../../firebase"
import { setShowTyping, updateMessages } from "../../redux/actions/chatActions"
import ChatInput from "../ChatInput/ChatInput"
import MessageBox from "../MessageBox/MessageBox"
import { selectChat } from "../../redux/actions/chatsActions"
import CompletedMessage from "../CompletedMessage/CompletedMessage"
import { usePubNub } from "pubnub-react"
import TypingIndicator from "../TypingIndicator/TypingIndicator"

const Chat = () => {
	const pubnub = usePubNub()
	const dispatch = useDispatch()
	const { chatId } = useParams()
	const uuid = useSelector(state => state.auth.credentials.user.uid)
	const messages = useSelector((state) => state.chat.messages)
	const selectedChat = useSelector((state) => state.chats.selectedChat)
	const showTyping = useSelector(state => state.chat.showTyping)

	const isCompleted = selectedChat ? selectedChat.status === "completed" : false

	useEffect(() => {
		pubnub.setUUID(uuid)
		pubnub.subscribe({
			channels: [chatId],
		})
		pubnub.addListener({signal: handleSignal})
	}, [chatId, pubnub, uuid]) // TODO fix this

	const handleSignal = (event) => {
		if (event.message === "typing_on_client"){
			dispatch(setShowTyping(true))
		}
		if (event.message === "typing_off_client") {
			dispatch(setShowTyping(false))
		}
	}

	useEffect(() => {
		const chatRef = firebase.database().ref(`chats/${chatId}`)
		chatRef.on("value", (snap) => {
			dispatch(selectChat(snap.val()))
		})
		chatRef.child("messages").on("value", (snap) => {
			dispatch(updateMessages(snap.val()))
		})
		return function () {
			chatRef.off()
		}
	}, [chatId, dispatch])

	const renderMessages = messages.map((message) => {
		return (
			<MessageBox
				key={message.timestamp + message.writtenBy}
				message={message}
			/>
		)
	})

	return (
		<div className="position-relative h-100 ">
			<div
				className="px-2 pt-2 d-flex flex-column-reverse"
				style={{ maxHeight: "100%", height: "90%", overflowY: "scroll" }}
			>
				{showTyping ? <TypingIndicator userName={selectedChat.clientName} /> : ""}
				{isCompleted ? <CompletedMessage rating={selectedChat.rating} /> : ""}
				{renderMessages.reverse()}
			</div>
			<ChatInput
				chatId={chatId}
				disabled={isCompleted}
			/>
		</div>
	)
}

export default Chat
