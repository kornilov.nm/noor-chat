import React from "react"
import SideBarButton from "../SideBarButton/SideBarButton"
import { resetPage, resetSelectedChat } from "../../redux/actions/chatsActions"
import { useDispatch } from "react-redux"
import QueueCounter from "../QueueCounter/QueueCounter"
import { NavLink } from "react-router-dom"

const SideNavBar = () => {
	const dispatch = useDispatch()

	const navLinkHandler = () => {
		dispatch(resetPage())
		dispatch(resetSelectedChat())
	}
	return(
		<div className="d-flex flex-column justify-content-around align-items-center">
			<div className="d-flex align-items-center">
				<NavLink to="/" onClick={navLinkHandler} className="d-flex text-link text-white">
					<div className="btn btn-primary d-flex flex-row justify-content-center text-center">
						<QueueCounter />
					</div>
				</NavLink>
			</div>
			<div className="d-flex justify-content-between">
				<SideBarButton navTo="/active" onClick={navLinkHandler} iconName="fa-comments">Active</SideBarButton>
				<SideBarButton navTo="/completed" onClick={navLinkHandler} iconName="fa-check-circle">Completed</SideBarButton>
				<SideBarButton navTo="/saved"  onClick={navLinkHandler} iconName="fa-bookmark">Saved</SideBarButton>
				{/*<SideBarButton navTo="/settings" iconName="fa-cogs">Settings</SideBarButton>*/}
			</div>
		</div>
	)
}
export default SideNavBar