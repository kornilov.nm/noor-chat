import React from "react"
import { Route, Switch, useRouteMatch } from "react-router-dom"
import ChatsQueue from "../ChatsTabs/ChatsQueue/ChatsQueue"
import ChatsActive from "../ChatsTabs/ChatsActive/ChatsActive"
import ChatsCompleted from "../ChatsTabs/ChatsCompleted/ChatsCompleted"
import ChatsSaved from "../ChatsTabs/ChatsSaved/ChatsSaved"
import SearchBar from "../SearchBar/SearchBar"
import SideNavBar from "../SideNavBar/SideNavBar"
import ChatsPagination from "../ChatsPagination/ChatsPagination"

const SideBar = () => {
	const { path } = useRouteMatch()

	return (
		<div className="bg-primary col-4 h-100 rounded-end shadow-lg p-1 w-100 d-flex flex-column text-light">

			<section style={{height: "20%"}}>
				<SearchBar className="my-1 d-flex align-items-end" />
				<SideNavBar />
			</section>

			<section style={{height: "80%"}} className="d-flex flex-column justify-content-between">
				<Switch>
					<Route exact path={`${path}`} component={ChatsQueue} />\
					<Route path={`${path}active`} component={ChatsActive} />
					<Route path={`${path}completed`} component={ChatsCompleted}/>
					<Route path={`${path}saved`} component={ChatsSaved} />
				</Switch>
				<ChatsPagination className="d-flex justify-content-between align-content-end" />
			</section>
		</div>
	)
}

export default SideBar
