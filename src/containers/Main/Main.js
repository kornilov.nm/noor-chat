import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Route, Switch, useHistory, useRouteMatch } from "react-router-dom"
import { verifyTokenRequest } from "../../redux/actions/authActions"
import SideBar from "../../components/SideBar/SideBar"
import UserBar from "../../components/UserBar/UserBar"
import Chat from "../../components/Chat/Chat"
import firebase from "../../firebase"
import { fetchChats, updateQueue } from "../../redux/actions/chatsActions"
import { toastError } from "../../services/toasters"

const Main = () => {
	const uid = useSelector((store) => store.auth.credentials.user.uid)
	const token = useSelector((store) => store.auth.token)
	const dispatch = useDispatch()
	const history = useHistory()
	const { path } = useRouteMatch()

	useEffect(() => {
		dispatch(verifyTokenRequest(uid, token, history))
	}, [dispatch, history, token, uid])

	useEffect(() => {
		const ref = firebase
			.database()
			.ref("chats")
			.orderByChild("status")
			.equalTo("pending")
		ref.on(
			"value",
			(snap) => {
				dispatch(updateQueue(snap.val()))
			},
			(error) => {
				toastError(error.message)
			}
		)
		return function () {
			ref.off()
		}
	}, [dispatch, uid])

	useEffect(() => {
		dispatch(fetchChats(uid, token))
	}, [dispatch, token, uid])

	return (
		<div className="row vh-100 bg-light">

			<section className="p-0 col-4 col-lg-3 h-100 d-none d-md-block">
				<SideBar />
			</section>

			<section className="col-12 col-md-8 col-lg-9 p-0 h-100">

				<UserBar
					className="h-20 p-2 m-0 shadow d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap"
					style={{ height: "15%" }}
				/>

				<div style={{ height: "85%" }}>
					<Switch>
						<Route exact path={`${path}active/:chatId`} component={Chat} />
						<Route exact path={`${path}completed/:chatId`} component={Chat} />
						<Route exact path={`${path}saved/:chatId`} component={Chat} />
					</Switch>
				</div>

			</section>

		</div>
	)
}

export default Main
