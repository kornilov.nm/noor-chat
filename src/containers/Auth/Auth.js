import React from "react"
import { Route, Switch, useRouteMatch } from "react-router-dom"
import SignInForm from "../../components/AuthForms/SignInForm/SignInForm"
import AuthCard from "../../hoc/AuthCard/AuthCard"
import SignUpForm from "../../components/AuthForms/SignUpForm/SignUpForm"
import ResetForm from "../../components/AuthForms/ResetForm/ResetForm"
import UpdateForm from "../../components/AuthForms/UpdateForm/UpdateForm"

const Auth = () => {
	const { path } = useRouteMatch()

	const authForms = (
		<Switch>
			<Route exact path={path} component={SignInForm} />
			<Route exact path={`${path}/signup`} component={SignUpForm} />
			<Route exact path={`${path}/recover`} component={ResetForm} />
			<Route exact path={`${path}/update`} component={UpdateForm} />
		</Switch>
	)
	return (
		<div className="row justify-content-center">
			<div className="col-lg-4 col-md-6 col-sm-12 mt-4">
				<AuthCard>{authForms}</AuthCard>
			</div>
		</div>
	)
}

export default Auth
