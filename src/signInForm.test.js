import React from "react"
import { shallow } from "`enzyme"
import SignInForm from "./components/AuthForms/SignInForm/SignInForm"
import configureStore from "redux-mock-store"
import { Provider } from "react-redux"

import classes from "./components/AuthForms/SignInForm/SignInForm.module.scss"

const middlewares = []
const mockStore = configureStore(middlewares)

const initialState = {}
const store = mockStore(initialState)

const setUp = (store) =>
	shallow(
		<Provider store={store}>
			<SignInForm />
		</Provider>
	)

describe("Sign in component", () => {
	let component
	beforeEach(() => {
		component = setUp(store)
	})

	test("should render with class", () => {
		const wrapper = component.find(classes.SignInForm)
		expect(wrapper).toHaveLength(1)
	})
})
