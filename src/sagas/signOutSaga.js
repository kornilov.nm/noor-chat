import { call, put, takeLatest } from "redux-saga/effects"
import { SIGN_OUT_REQUEST, SIGN_OUT_SUCCESS } from "../redux/types/typesAuth"
import { toastError } from "../services/toasters"

export function* watchSignOutSaga() {
	yield takeLatest(SIGN_OUT_REQUEST, signOutSaga)
}

function* signOutSaga(action) {
	const history = action.history
	try {
		yield call(resetStorage)
		yield call(history.push, "/auth")
		yield put({ type: SIGN_OUT_SUCCESS })
		toastError("You was logged out")
	} catch (error) {
		toastError(`It's impossible but ${error.message}`)
	}
}

function resetStorage() {
	localStorage.clear()
}
