import { call, put, takeLatest } from "redux-saga/effects"
import {
	FETCH_CHATS_ERROR,
	FETCH_CHATS_REQUEST,
	FETCH_CHATS_SUCCESS,
	HIDE_LOADING,
	SHOW_LOADING,
	START_CHAT_ERROR,
	START_CHAT_REQUEST,
	START_CHAT_SUCCESS,
	TOGGLE_SAVED_ERROR,
	TOGGLE_SAVED_REQUEST,
	TOGGLE_SAVED_SUCCESS,
} from "../redux/types/typesChats"
import {
	fetchChatsFirebase,
	patchChatFirebase,
	startChatFirebase,
} from "../services/databaseMehtods"
import { toastError } from "../services/toasters"

export function* watchFetchChats() {
	yield takeLatest(FETCH_CHATS_REQUEST, fetchChatsSaga)
	yield takeLatest(TOGGLE_SAVED_REQUEST, toggleSavedSaga)
	yield takeLatest(START_CHAT_REQUEST, startChatSaga)
}

function* fetchChatsSaga(action) {
	const uid = action.uid
	const token = action.token
	yield put({ type: SHOW_LOADING })
	try {
		const chats = yield call(fetchChatsFirebase, uid, token)
		yield put({ type: FETCH_CHATS_SUCCESS, chats: chats.data })
		yield put({ type: HIDE_LOADING })
	} catch (e) {
		yield put({ type: HIDE_LOADING })
		yield put({ type: FETCH_CHATS_ERROR, error: e })
	}
}
function* toggleSavedSaga(action) {
	const newChats = action.chats
	const chatId = action.chatId
	const newValue = newChats[chatId].saved
	const token = action.token
	try {
		yield call(patchChatFirebase, chatId, newValue, token)
		yield put({ type: TOGGLE_SAVED_SUCCESS, chats: newChats })
	} catch (e) {
		yield put({ type: TOGGLE_SAVED_ERROR, error: e })
		toastError(e.message)
	}
}

function* startChatSaga(action) {
	const token = action.token
	const chatId = action.chatId
	const operatorId = action.uid
	const history = action.history
	const newChatsState = action.newChatsState
	const newQueueState = action.newQueueState
	try {
		yield call(startChatFirebase, chatId, operatorId, token)
		yield put({
			type: START_CHAT_SUCCESS,
			chats: newChatsState,
			queue: newQueueState,
		})
		history.push(`active/${chatId}`)
	} catch (e) {
		toastError(e.message)
		yield put({ type: START_CHAT_ERROR, error: e })
	}
}
