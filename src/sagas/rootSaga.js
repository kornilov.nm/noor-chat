import { all } from "redux-saga/effects"
import { watchAuthenticationSaga } from "./authSagas"
import { watchResetPasswordSaga } from "./resetPasswordSaga"
import { watchVerificationSaga } from "./verificationSaga"
import { watchSignOutSaga } from "./signOutSaga"
import { watchFetchChats } from "./chatsSaga"

export default function* rootSaga() {
	yield all([
		watchAuthenticationSaga(),
		watchResetPasswordSaga(),
		watchVerificationSaga(),
		watchSignOutSaga(),
		watchFetchChats(),
	])
}
