import { call, put, takeLatest } from "redux-saga/effects"
import {
	HIDE_LOADING,
	RESET_PASSWORD_REQUEST,
	SHOW_LOADING,
	VERIFY_PASSWORD_REQUEST,
} from "../redux/types/typesAuth"
import {
	confirmPasswordReset,
	resetPassword,
	verifyPasswordResetCode,
} from "../services/authMethods"
import { toastError, toastSuccess } from "../services/toasters"

export function* watchResetPasswordSaga() {
	yield takeLatest(RESET_PASSWORD_REQUEST, resetSaga)
	yield takeLatest(VERIFY_PASSWORD_REQUEST, verifySaga)
}
function* resetSaga(action) {
	yield put({ type: SHOW_LOADING })
	try {
		yield call(resetPassword, action.userEmail)
		yield call(
			toastSuccess,
			`Password reset link was sent to ${action.userEmail}`
		)
		yield put({ type: HIDE_LOADING })
	} catch (error) {
		yield call(toastError, error.message)
		yield put({ type: HIDE_LOADING })
	}
}

function* verifySaga(action) {
	const history = action.history
	yield put({ type: SHOW_LOADING })
	try {
		const email = yield call(verifyPasswordResetCode, action.actionCode)
		try {
			yield call(confirmPasswordReset, action.actionCode, action.newPassword)
			yield put({ type: HIDE_LOADING })
			yield call(history.push, "/auth")
			yield call(
				toastSuccess,
				`Password was successfully reset for email: ${email}`
			)
		} catch (error) {
			yield call(toastError, error.message)
			yield put({ type: HIDE_LOADING })
		}
	} catch (error) {
		yield call(toastError, error.message)
		yield put({ type: HIDE_LOADING })
	}
}
