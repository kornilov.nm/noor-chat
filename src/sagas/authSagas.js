import { call, put, takeLatest } from "redux-saga/effects"
import {
	HIDE_LOADING,
	LOGIN_ERROR,
	LOGIN_REQUEST,
	LOGIN_SUCCESS,
	REGISTER_ERROR,
	REGISTER_REQUEST,
	REGISTER_SUCCESS,
	RESET_ERROR,
	SHOW_LOADING,
	SOCIAL_MEDIA_AUTH_REQUEST,
} from "../redux/types/typesAuth"
import { toastError, toastSuccess } from "../services/toasters"
import {
	firebaseLogin,
	firebaseRegister,
	getToken,
	socialMediaAuth,
} from "../services/authMethods"

export function* watchAuthenticationSaga() {
	yield takeLatest(LOGIN_REQUEST, loginSaga)
	yield takeLatest(REGISTER_REQUEST, registerSaga)
	yield takeLatest(SOCIAL_MEDIA_AUTH_REQUEST, loginSaga)
}

function* loginSaga(action) {
	const history = action.history
	yield put({ type: SHOW_LOADING })
	yield put({ type: RESET_ERROR })
	try {
		let userCredentials
		let token
		if (action.user) {
			userCredentials = yield call(firebaseLogin, action.user)
		}
		if (action.mediaProvider) {
			userCredentials = yield call(socialMediaAuth, action.mediaProvider)
		}
		try {
			token = yield call(getToken)
		} catch (error) {
			toastError(error.message)
		}
		yield put({ type: LOGIN_SUCCESS, userCredentials, token })
		yield put({ type: HIDE_LOADING })
		yield call(
			toastSuccess,
			`You have successfully logged in with ${userCredentials.user.email}`
		)
		yield call(history.push, "/")
	} catch (error) {
		yield put({ type: HIDE_LOADING })
		yield put({ type: LOGIN_ERROR, error })
		toastError(error.message)
	}
}

function* registerSaga(action) {
	const history = action.history
	yield put({ type: SHOW_LOADING })
	yield put({ type: RESET_ERROR })
	try {
		const registeredCredential = yield call(firebaseRegister, action.newUser)
		yield put({ type: REGISTER_SUCCESS })
		yield put({ type: HIDE_LOADING })
		yield call(history.push, "/auth")
		yield call(
			toastSuccess,
			`You have been successfully registered with ${registeredCredential.user.email}`
		)
	} catch (error) {
		yield put({ type: HIDE_LOADING })
		yield put({ type: REGISTER_ERROR, error })
		toastError(error.message)
	}
}

