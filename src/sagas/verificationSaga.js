import { call, put, takeLatest } from "redux-saga/effects"
import { SIGN_OUT_REQUEST, VERIFY_TOKEN_REQUEST } from "../redux/types/typesAuth"
import { verifyToken } from "../services/authMethods"
import { toastError } from "../services/toasters"

export function* watchVerificationSaga() {
	yield takeLatest(VERIFY_TOKEN_REQUEST, verifyTokenSaga)
}

function* verifyTokenSaga(action) {
	const history = action.history
	try {
		yield call(verifyToken, action.uid, action.token)
	} catch (error) {
		yield put({ type: SIGN_OUT_REQUEST, history })
		toastError(error.message)
	}
}
