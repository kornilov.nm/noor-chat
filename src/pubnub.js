import PubNub from "pubnub"

const pubnub = new PubNub({
	publishKey: process.env.REACT_APP_PUB_PUBLISH_KEY,
	subscribeKey: process.env.REACT_APP_PUB_SUBSCRIBE_KEY,
})

export default pubnub